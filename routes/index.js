var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  var db = req.db;
  var posts = db.get('posts');
  posts.find({},{sort: {date: 1}}, function(err, posts) {
    res.render('index', { posts: posts, blogpost: false });
  });

});

router.get('/entry', function(req, res, next) {
  res.render('entry', { title: 'Express', blogpost: true, back: true });
});

module.exports = router;
