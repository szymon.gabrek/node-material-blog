var express = require('express');
var router = express.Router();
var multer = require('multer');
var upload = multer({ dest: 'uploads/' });
var mongo = require('mongodb')
var db = require('monk')('localhost/node-material-blog');
var posts = db.get('posts');
var categories = db.get('categories');

router.get('/add', function(req, res, next) {
  console.log(categories);
  categories.find({},{}, function(err, categories) {
    res.render('add', {
      title: 'Add New Post',
      blogpost: true,
      back: true,
      categories: categories,
      category: "Technology"
    });
  });
});

router.post('/add', upload.single('mainimage'),  function(req, res, next) {
  // Get the form values
  console.log(req.query)
  if(req.file){
    var mainimage = req.file.filename;
  }else{
    var mainimage = '';
  }
  var title = req.body.title;
  var color = req.body.color;
  var category = req.body.category;
  var body = req.body.body;
  var author = req.body.author;
  var date = new Date();

  req.checkBody('title', 'Title field is required').notEmpty();
  req.checkBody('body', 'Body field is required').notEmpty();
  req.checkBody('author', 'Author field is required').notEmpty();

  // Check Errors
  var errors = req.validationErrors();

  if(errors){
    categories.find({},{}, function(err, categories) {
      res.render('add', {
        "errors": errors,
        "title": title,
        "author": author,
        "body": body,
        "blogpost": true,
        "back": true,
        "image": mainimage,
        "category": category,
        "categories": categories,
      });
    });
  }else{
    posts.insert({
      "title": title,
      "body": body,
      "category": category,
      "date": date,
      "author": author,
      "image": mainimage,
      "color": color
    }, function(err, post) {
      if(err){
        res.send(err);
      }else{
        req.flash('success', 'Post Added');
        res.location('/');
        res.redirect('/');
      }
    });
  }

});

router.get('/delete/:id', upload.none() ,  function(req, res, next) {
  posts.remove(
            {_id: new mongo.ObjectID( req.params.id) },
            function (err, result){
               if(err){
                 res.send(result);
               }else{
                 req.flash('success', 'Post Removed');
                 res.location('/');
                 res.redirect('/');
               }
              });

  res.redirect('/');

});

router.get('/show/:id', upload.none() ,  function(req, res, next) {
  posts.findOne({ _id: new mongo.ObjectID( req.params.id)  }).then((post)=>{
  // res.send(post.body)
    res.render('entry', {
      title: post.title,
      blogpost: true,
      back: true,
      color: post.color,
      author: post.author,
      date: post.date,
      body: post.body,
      image: post.image

     });
  });

});

router.get('/edit/:id', upload.none() ,  function(req, res, next) {

  posts.remove(
            {_id: new mongo.ObjectID( req.params.id) },
            function (err, result){
               if(err){
                 res.send(result);
               }else{
                 req.flash('success', 'Post Removed');
                 res.location('/');
                 res.redirect('/');
               }
              });

  res.redirect('/');

});


module.exports = router;
