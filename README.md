# Node Material Blog
> Simple Express Material Design Lite Blog Experiment

## Installation
```sh
npm install  
npm start
```
## Built With

* [Express](https://github.com/expressjs) - The web framework used
* [Mongodb](https://www.mongodb.com/) - Database Used
* [Node.js](https://nodejs.org/) -  run-time environment
* [Jademine](https://jasmine.github.io/) -  Testing

## License

  This project is licensed under the MIT License
