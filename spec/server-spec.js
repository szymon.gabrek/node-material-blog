var request = require("request");
var helloWorld = require("../bin/www")

describe("Server", function() {
  describe("Server Start", function() {
    beforeEach(() => {
        request.ignoreSynchronization = true;
        request
              .get('http://localhost:3000')
              .on('response', function(response) {
                console.log(response.statusCode) // 200
                console.log(response.headers['content-type']) // 'image/png'
              });
    });
    it("returns status code 200", function(done) {
      request.get("http://localhost:3000", function(error, response, body) {
        expect(response.statusCode).toBe(200);
        done();
      });
    });

  });

  //TODO: Add Post Test

  //TODO: Remove Post Test

  //TODO: Edit Post Test

});
